<?php

namespace App\Http\Controllers\Home;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
    	return view("Home.index.index");
    }

    public function test(){
  //   	$host = '127.0.0.1';
		// $port = 9090;
		// $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP) or die("socket_create() 失败:".socket_strerror(socket_last_error())."\n");
		// $ret = socket_connect($socket, $host, $port) or die("socket_connect() 失败:".socket_strerror(socket_last_error())."\n");
		// $msg = ['fromUser'=>'Alice','msg'=>'test','type'=>'chat'];;
		// $msg = json_encode($msg);
		// socket_write($socket, $msg, strlen($msg));
		// $data = socket_read($socket, 4096);
		// echo "server回复:".$data."\n";
		// socket_close($socket);
    	return view("Home.index.test");
    }

    public function chat(){
    	return view("Home.index.chat");
    }

    public function chatRoom(){
        return view("Home.index.chatRoom");
    }
}
