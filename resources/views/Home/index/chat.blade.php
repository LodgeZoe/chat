<!DOCTYPE html>
<html lang="en" >
<head>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-title" content="CodePen">
<title>CodePen - Daily UI #013 - Direct Messaging</title>

<link rel="apple-touch-icon" type="image/png" href="/chatRoom/images/apple-touch-icon.png" />
<link rel="shortcut icon" type="image/x-icon" href="/chatRoom/images/favicon.ico" />
<link rel="mask-icon" type="" href="/chatRoom/images/logo-pin.svg" color="#111" />

<link rel='stylesheet' href='/css/fontawesome.v5.0.13.all.css'>  
<link rel='stylesheet' href='/css/testChat.css'>  

<script data-cfasync="false" src="/js/email-decode.min.js"></script>
<script src='/chatRoom/js/1.3.14_angular.min.js'></script>
<script src='/chatRoom/js/2.1.3_jquery.min.js'></script>
<script src='/chatRoom/js/chat.js'></script>
<script>
  window.console = window.console || function(t) {};
</script>
  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
</script>
</head>
<body translate="no" >
<div class="container" ng-cloak ng-app="chatApp">
  <div class="chatbox" ng-controller="MessageCtrl as chatMessage">
    <div class="top-bar">
      <span class="avatar1"><p>V</p></span>
      <span class="name">Voldemort</span>
      <!-- <div class="icons">
        <i class="fas fa-phone"></i>
        <i class="fas fa-video"></i>
      </div>
      <div class="menu">
        <div class="dots"></div>
      </div> -->
    </div>
    <div class="middle" id="message_list">
        <div class="message <%message.className%>" ng-repeat="message in messages">
          <p class="bubble"><%message.msg%></p>
        </div>
    </div>
    <div class="bottom-bar">
        <input type="text" ng-model="input" autocomplete="off" ng-keydown="checkSend()" placeholder="Type a message..." />
        <button id="send" ng-click="send()" value="Send">
            <!-- <i class="fas fa-paper-plane"></i> -->
            Send
        </button>
    </div>
  </div>
  <div class="messages">
    <div class="profile">
        <div class="avatar"><p>H</p></div>
        <div class="name2">Harry<p class="email">
            <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="226a4350505b62524d565647500c414d4f">[email&#160;protected]</a></p>
        </div>
    </div>
      <ul class="people">
        <li class="person focus">
          <span class="title">Voldemort </span>
          <span class="time">2:50pm</span><br>
          <span class="preview">What are you getting... Oh, oops...</span>
        </li>
         <li class="person">
          <span class="title">Ron</span>
          <span class="time">2:25pm</span><br>
          <span class="preview">Meet me at Hogsmeade and bring...</span>
        </li>
        <li class="person">
          <span class="title">Hermione</span>
          <span class="time">2:12pm</span><br>
          <span class="preview">Have you and Ron done your hom...</span>
        </li>
      </ul>
  </div>
</div>

<footer>
  <!-- <p>made by <a href="#"> julie</a> ♡ -->
</footer>
<script id="rendered-js" >
$(".person").on('click', function () {
  $(this).toggleClass('focus').siblings().removeClass('focus');
});
</script>
</body>

</html>