<!DOCTYPE html>
<html lang="en" >
<head>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-title" content="CodePen">
<title>DawnCity ChatRoom</title>
<link rel="apple-touch-icon" type="image/png" href="/chatRoom/images/apple-touch-icon.png" />
<link rel="shortcut icon" type="image/x-icon" href="/chatRoom/images/favicon.ico" />
<link rel="mask-icon"href="/chatRoom/images/logo-pin.svg" color="#111" />
<link href='/chatRoom/css/googleapis.css' rel='stylesheet' type='text/css'>
<link href='/chatRoom/css/chat.css' rel='stylesheet' type='text/css'>
<script src="/js/stopExecutionOnTimeout.js"></script>
<script src='/chatRoom/js/1.3.14_angular.min.js'></script>
<script src='/chatRoom/js/2.1.3_jquery.min.js'></script>
<script src='/chatRoom/js/chat.js'></script>
<script>
  window.console = window.console || function(t) {};
</script>  
<script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
</script>
</head>
<body translate="no" >
  <div class='container' ng-cloak ng-app="chatApp">
  <h1>WelCome To DawnCity ChatRoom</h1>
  <div class='chatbox' ng-controller="MessageCtrl as chatMessage">
    <div class='chatbox__user-list'>
      <h1>User list</h1>
      <div class='chatbox__user--active'>
        <p>Jack Thomson</p>
      </div>
      <div class='chatbox__user--busy'>
        <p>Angelina Jolie</p>
      </div>
      <div class='chatbox__user--active'>
        <p>George Clooney</p>
      </div>
      <div class='chatbox__user--active'>
        <p>Seth Rogen</p>
      </div>
      <div class='chatbox__user--away'>
        <p>John Lydon</p>
      </div>
    </div>
    <div class="chatbox__message-list" id="message_list">
      <div class="chatbox__messages" ng-repeat="message in messages">
        <div class="chatbox__messages__user-message <%message.className%>">
          <div class="chatbox__messages__user-message--ind-message">
            <p class="name"><%message.fromUser%></p>
            <br/>
            <p class="message"><%message.msg%></p>
          </div>
        </div>
      </div>
    </div>
    <form>
      <input type="text" id="content" ng-model="input" autocomplete="off" ng-keydown="checkSend()" placeholder="Enter your message">
    </form>
  </div>
</div>
<script id="rendered-js" >
if(typeof(WebSocket)=='undefined'){
  alert('你的浏览器不支持 WebSocket ，推荐使用Google Chrome 或者 Mozilla Firefox');  
}
//# sourceURL=pen.js
</script>
</body>
</html>