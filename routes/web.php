<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Home\IndexController;


Route::get('/', function () {
    return view('Home.index.index');
});

Route::get('/greeting', function () {
    return 'Hello World';
});
// Route::get('/test', function () {
//     echo 'here';
// });
Route::get('/test', [IndexController::class, 'test']); 
Route::get('/index',[IndexController::class, 'index']);
Route::get('/chat',[IndexController::class, 'chat']);
Route::get('/Home/chatRoom',[IndexController::class, 'chatRoom']);
// Route::get('/','IndexController@index');
// 前台路由
// Route::group(['prefix'=>'Home'],function(){
// 	Route::get('test', [IndexController::class, 'test']); 
// 	Route::get('index',[IndexController::class, 'index']);
// 	Route::get('test','IndexController@test');
// 	Route::get('user',function(){
// 		// 此方法会匹配 /home/user 路径
// 		echo 'here is /home/user';
// 	});
// });

// 后台路由
// Route::group(['prefix'=>'Admin'],function(){
// 	Route::get('/','Admin\IndexController@index')->middleware('CheckLogin');
// 	Route::get('/index','Admin\IndexController@index')->middleware('CheckLogin');
	
// 	Route::get('login','Admin\IndexController@login');
// 	Route::post('loginCheck','Admin\IndexController@loginCheck');
// 	Route::get('cssControl','Admin\IndexController@cssControl');
// 	Route::get('getBat','Admin\IndexController@getBat');
// });