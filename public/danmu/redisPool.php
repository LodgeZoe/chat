<?php
class RedisPool
{
    /**@var \Swoole\Coroutine\Channel */
    protected $pool;
    static private $instance;
    public $poolSize = 64;
    /**
     * RedisPool constructor.
     * @param int $size max connections
     */
    private function __construct()
    {
        $this->pool = new \Swoole\Coroutine\Channel($this->poolSize);
        for ($i = 0; $i < $this->poolSize; $i++) {
            go(function(){
                $redis = new \Swoole\Coroutine\Redis();
                $res = $redis->connect('127.0.0.1', 6379);
                if ($res == false) {
                    throw new \RuntimeException("failed to connect redis server.");
                } else {
                    $this->put($redis);
                }

            });
        }
    }

    private function __clone(){}

    static public function getInstance(){
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function get(): \Swoole\Coroutine\Redis
    {
        return $this->pool->pop();
    }

    public function put(\Swoole\Coroutine\Redis $redis)
    {
        $this->pool->push($redis);
    }

    public function close(): void
    {
        $this->pool->close();
        $this->pool = null;
    }
}

// go(function () {
//     $pool = new RedisPool();
//     // max concurrency num is more than max connections
//     // but it's no problem, channel will help you with scheduling
//     for ($c = 0; $c < 1000; $c++) {
//         go(function () use ($pool, $c) {
//             for ($n = 0; $n < 100; $n++) {
//                 $redis = $pool->get();
//                 assert($redis->set("awesome-{$c}-{$n}", 'swoole'));
//                 assert($redis->get("awesome-{$c}-{$n}") === 'swoole');
//                 assert($redis->delete("awesome-{$c}-{$n}"));
//                 echo 'done';
//                 $pool->put($redis);
//             }
//         });
//     }
//     sleep(100);
// });
