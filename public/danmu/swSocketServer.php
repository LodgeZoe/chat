<?php
use Swoole\Coroutine;
use Swoole\Database\RedisConfig;
// use Swoole\Database\RedisPool;
use Swoole\Runtime;
// Runtime::enableCoroutine();
include_once('redisPool.php');
class Server
{
    private $serv;
    private $redisPool;
    private $redisPool1;
    private $redisPoolSize = 64;
    private $redis;
    // swoole 是可以监听多端口，具体查看 https://github.com/LodgeZoe/Swoole/blob/master/06-Swoole%20%E5%A4%9A%E5%8D%8F%E8%AE%AE%20%E5%A4%9A%E7%AB%AF%E5%8F%A3%20%E7%9A%84%E5%BA%94%E7%94%A8.md
    public function __construct() {
        $this->serv = new Swoole\WebSocket\Server("0.0.0.0", 9090);
        // $this->serv = new swoole_websocket_server("0.0.0.0", 9090);
        $this->serv->set([
            'worker_num'      => 2, //开启2个worker进程
            'max_request'     => 4, //每个worker进程 max_request设置为4次
            'task_worker_num' => 4, //开启4个task进程
            'dispatch_mode'   => 4, //数据包分发策略 - IP分配
            'daemonize'       => true, //守护进程(true/false)

            //EOF结束符协议
            'package_max_length' => '8192',
            'open_eof_split'     => true,
            'package_eof'        => "\r\n",

            // 固定包头+包体协议
            // 'open_length_check'     => true,
            // 'package_max_length'    => '8192',
            // 'package_length_type'   => 'N',
            // 'package_length_offset' => '0',
            // 'package_body_offset'   => '4',

            'enable_coroutine'      => true,
            'task_enable_coroutine'   => true

        ]);
        $this->serv->on('Start', [$this, 'onStart']);
        // $this->serv->on('ManagerStart', [$this, 'onManagerStart']);
        // $this->serv->on('WorkerStart', [$this, 'onWorkerStart']);
        $this->serv->on('Open', [$this, 'onOpen']);
        $this->serv->on("Message", [$this, 'onMessage']);
        $this->serv->on("Close", [$this, 'onClose']);
        $this->serv->on("Task", [$this, 'onTask']);
        $this->serv->on("Finish", [$this, 'onFinish']);

        $this->serv->start();
    }

    public function onStart($serv) {
        echo "#### onStart ####".PHP_EOL;
        echo "SWOOLE ".SWOOLE_VERSION . " 服务已启动".PHP_EOL;
        echo "master_pid: {$serv->master_pid}".PHP_EOL;
        echo "manager_pid: {$serv->manager_pid}".PHP_EOL;
        RedisPool::getInstance();
        echo "########".PHP_EOL.PHP_EOL;
    }

    // public function onWorkerStart($serv,$worker_id) {
    //     // global $argv;
    //     // if($worker_id >= $server->setting['worker_num']) {
    //     //     swoole_set_process_name("php {$argv[0]} task worker");
    //     // } else {
    //     //     swoole_set_process_name("php {$argv[0]} event worker");
    //     // }
    //     echo "#### onWorkerStart ####".PHP_EOL;
    //     echo "RedisPool 初始化".PHP_EOL;
    //     echo "worder_id: {$worker_id}".PHP_EOL;
    //     // RedisPool::getInstance();
    //     echo "manager_pid: {$serv->manager_pid}".PHP_EOL;
    //     echo "########".PHP_EOL.PHP_EOL;
    // }

    public function onOpen($serv, $request) {
        echo "#### onOpen ####".PHP_EOL;
        echo "server: handshake success with fd{$request->fd}".PHP_EOL;
        // $data = ['fromUser'=>'Alice','msg'=>'test','type'=>'chat'];
        // $redis = $this->redis;
        // $redis->set($data['fromUser'],$request->fd);
        $serv->task([
            'type' => 'login'
        ]);
        echo "########".PHP_EOL.PHP_EOL;
    }

    public function onTask($serv,Swoole\Server\Task $task) {
        // $task->worker_id; //来自哪个`Worker`进程
        $task_id = $task->id; //任务的编号
        $data = $task->data; //任务的数据
        echo "#### onTask ####".PHP_EOL;
        echo "#{$serv->worker_id} onTask: [PID={$serv->worker_pid}]: task_id={$task_id}".PHP_EOL;
        $msg = '';
        switch ($data['type']) {
            case 'login':
                $msg = '我来了...';
                break;
            case 'speak':
                $msg = $data['msg'];
                break;
        }
        foreach ($serv->connections as $fd) {
            $connectionInfo = $serv->connection_info($fd);
            if ($connectionInfo['websocket_status'] == 3) {
                $serv->push($fd, $msg); //长度最大不得超过2M
            }
        }
        $task->finish($data);
        echo "########".PHP_EOL.PHP_EOL;
    }

    public function onFinish($serv,$task_id, $data) {
        echo "#### onFinish ####".PHP_EOL;
        echo "Task {$task_id} 已完成".PHP_EOL;
        echo "########".PHP_EOL.PHP_EOL;
    }

    public function onMessage($serv, $frame) {
        echo "#### onMessage PID:$serv->worker_pid####".PHP_EOL;
        $data = json_decode(stripslashes($frame->data),true);
        // $data = ['fromUser'=>'Alice','msg'=>'test','type'=>'chat'];
        echo "receive from fd{$frame->fd}:{$data['fromUser']},opcode:{$frame->opcode},fin:{$frame->finish}\ndata:{$frame->data}".PHP_EOL;
        // $redis = $this->redisPool->get();
        // $redis = new Redis();
        // $redis->connect('127.0.0.1', 6379);
        // $redis = $this->redis;
        // $pool = RedisPool::getInstance();
        // $redis = $pool->get();
        $redis = RedisPool::getInstance()->get();
        // echo "connections:".json_encode($serv->connections).PHP_EOL;
        $redis->set($data['fromUser'],$frame->data);
        $pool->put($redis);
        if(($data['type']??'')=='chat'){
            foreach ($serv->connections as $fd) {
                $connectionInfo = $serv->connection_info($fd);
                if ($connectionInfo['websocket_status'] == 3) {
                    $serv->push($fd, json_encode($data,256)); //长度最大不得超过2M
                }
            }
        }else{
            $serv->task(['type' => 'speak', 'msg' => $frame->data]);
        }
        echo "########".PHP_EOL.PHP_EOL;
    }

    public function onClose($serv, $fd) {
        echo "#### onClose ####".PHP_EOL;
        // $data = ['fromUser'=>'Alice','msg'=>'test','type'=>'chat'];
        // $redis = $this->redis;
        // $redis->set($data['fromUser'],$fd);
        echo "client {$fd} closed".PHP_EOL;
        echo "########".PHP_EOL.PHP_EOL;
    }
}

$server = new Server();
