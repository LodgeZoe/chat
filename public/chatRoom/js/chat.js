(function () {
  var app = angular.module('chatApp', [],function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
  });
  app.controller('MessageCtrl', function ($scope) {
    $scope.messages = [
      {
        fromUser: 'System Message',
        type:'',
        msg: "Hey dude, how's recently going!",
        className: "message-left"
      }
    ];
    $scope.input = '';
    $scope.getRandName = ()=>{
      let username = localStorage.getItem('username');
      if(username==undefined||username==''||username==null){
        let Arr1 = ["Tom","Jerry","Alice","Ted","Alan","Steve","Angelina","Elizabeth"]; 
        // let Arr2 = ["大灰狼","小白兔","母老虎","外星人","皮卡丘","HelloKitty","吴亦凡","薛之谦"]; 
        let ran1 = Math.floor(Math.random() * Arr1.length + 1)-1;  
        // let ran2 = Math.floor(Math.random() * Arr2.length + 1)-1;  
        // let n=Arr1[ran1]+Arr2[ran2];
        let n=Arr1[ran1];
        localStorage.setItem('username',n);
        return n;
      }
      return username;
    }
    $scope.username = $scope.getRandName();
    $scope.ws = new WebSocket("ws://www.wolegqu.cn:9090");
    $scope.ws.onopen = (evt)=>{
      let msg = $scope.ws.readyState==1?'WebSocket 连接成功...':'WebSocket 连接失败...';
      console.log(msg);
    }
    $scope.ws.onmessage = (evt)=>{
      console.log('Retrieved data from server: ' + evt.data);
      if(evt.data=="我来了...") return;
      let data = JSON.parse(evt.data);
      data.className = "message-left";
      if(data.fromUser==$scope.username) data.className = "message-right";
      $scope.messages.push(data);
      $scope.$apply();
      let element = document.getElementById('message_list');
      element.scrollTop = element.scrollHeight;
    };
    $scope.send = ()=>{
      if ($scope.ws.readyState == 1) {
          let data = {};
          data.fromUser = $scope.username;
          if($scope.input=='') return ;
          if($scope.input=='test'){
              for (var i = 1000; i >= 0; i--) {
                  let date = new Date();
                  let time = '';
                  time += i+'=>'+date.getSeconds()+'.'+date.getMilliseconds();
                  let data = {fromUser:'Alice',msg:time,type:'chat'};
                  let str = JSON.stringify(data);
                  console.log("sendData:"+str);
                  $scope.ws.send(str);
              }  return ;
          } 
          data.msg = $scope.input;
          $scope.input = '';
          data.type = "chat";
          $scope.ws.send(JSON.stringify(data));
      } else {
        alert('WebSocket 连接失败');
      }
    };
    $scope.checkSend = ()=>{
      let e = window.event;
      if (e.keyCode == 13) {
        $scope.send();
        document.getElementById('content').focus();
      }
    }
  });
})();